import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import App from './app'
import Fighter from './fighter'

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  fightersMap = new Map();
  fightersFighte = new Map();
  
  isModalOpen;  

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);      

      const selectFighterBtn = this.createElement( {tagName: 'button', className: 'fighter__btn'})
      selectFighterBtn.innerHTML = 'Select Fighter';
      
      selectFighterBtn.addEventListener('click', () => {this.selectFghiters(fighter, selectFighterBtn)} , false);
      fighterView.element.append(selectFighterBtn);

      return fighterView.element; 
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    const result = this.fightersMap.get(fighter._id);

    if (result) {
      this.createModal(result)
      return;
    }

    const fighterResponse = await fighterService.getFighterDetails(fighter._id);
    this.fightersMap.set(fighter._id, fighterResponse);

    this.createModal(fighterResponse);
  }


  createModal(fighter) {
    if (this.isModalOpen) {
      const modal = document.querySelector('.modal');
      App.rootElement.removeChild(modal);
    }

    this.isModalOpen = true;

    const ulElement = this.createElement({ tagName: 'ul', className: 'modal' });

    for (let key in fighter) {
      if (['health', 'attack', 'defense'].includes(key)) {
        const liCreateElement = this.createElement({ tagName: 'li', className: 'modal__item' });
        liCreateElement.innerHTML = `<b>${key}</b> - <span>${fighter[key]}</span>`;
        this.editText(liCreateElement.lastElementChild, fighter, key)
        ulElement.append(liCreateElement);
      }
    }

    this.createCloseButton(ulElement);
    App.rootElement.appendChild(ulElement);
  }

  createCloseButton(modal) {
    const closeButton = this.createElement({ tagName: 'button', className: 'modal__close' });
    modal.append(closeButton);
    this.closeModalListener(closeButton, modal);
  }

  closeModalListener(button, modal) {
    button.addEventListener('click', (event) => {
      App.rootElement.removeChild(modal);
      this.isModalOpen = false;
    }, false);
    
  }

  editText = (element, fighter, key) => {
    const editFinther = () => {
      const createInput = this.createElement({ tagName: 'input', className: 'modal__edit', attributes: { date: key }})
      createInput.value = element.innerHTML;
      element.innerHTML = '';
      element.append(createInput);
      element.removeEventListener('click', editFinther)

      createInput.addEventListener('blur', (event) => {
        if( event.target.attributes.date.value !== 'name' && isNaN(event.target.value) ) {
          element.innerHTML = fighter[event.target.attributes.date.value];
        } else {
          element.innerHTML = createInput.value; 
          const val = this.fightersMap.get(fighter._id);

          val[event.target.attributes.date.value] = event.target.value;
        }      
        element.addEventListener('click', editFinther)
      })
    }  

    element.addEventListener('click', editFinther)
  }
  
  createStart() {
    const startBtn = this.createElement( {tagName: 'button', className: 'start'} )
    startBtn.addEventListener('click', async () => {await this.startFight()})
    startBtn.innerHTML = 'Start Game';
    
    App.rootElement.append(startBtn);
  }
  
  async startFight() {
    const keys = [...this.fightersFighte.keys()];
    let fighterParam1;
    let fighterParam2;

    if(this.fightersMap.get(keys[0])) {
      const fighterResp1 = this.fightersMap.get(keys[0]);
      fighterParam1 = new Fighter(fighterResp1.name, fighterResp1.health, fighterResp1.attack, fighterResp1.defense);
    } else {
      const fighterResponse1 = await fighterService.getFighterDetails(keys[0]);
      fighterParam1 = new Fighter(fighterResponse1.name, fighterResponse1.health, fighterResponse1.attack, fighterResponse1.defense);
    }

    if(this.fightersMap.get(keys[1])) {
      const fighterResp2 = this.fightersMap.get(keys[1]);
      fighterParam2 = new Fighter(fighterResp2.name, fighterResp2.health, fighterResp2.attack, fighterResp2.defense);
    } else {
      const fighterResponse2 = await fighterService.getFighterDetails(keys[1]);
      fighterParam2 = new Fighter(fighterResponse2.name, fighterResponse2.health, fighterResponse2.attack, fighterResponse2.defense);
    } 

    const winner = Fighter.fight(fighterParam1, fighterParam2);
    
   alert(`Winner Name: ${winner}`)
  }

  selectFghiters(fighter, btn) {
    const allFighters = document.querySelectorAll('.fighter__btn');

    if (this.fightersFighte.has(fighter._id)) {
      this.fightersFighte.delete(fighter._id);
      btn.classList.remove('active');
      btn.innerHTML = 'Select Fighter';
    } else {
      this.fightersFighte.set(fighter._id, fighter);   
      btn.classList.toggle('active');
      btn.innerHTML = 'Active'
    }

    if (this.fightersFighte.size === 2) {
      this.createStart();

      allFighters.forEach(item => {      
        if(item.className !== 'fighter__btn active') {
          item.classList.add('none')
          item.disabled = true
        }
      })
    } else {
      this.removeStratBtn()
      allFighters.forEach(item => {      
        if(item.className !== 'fighter__btn active') {
          item.classList.remove('none')
          item.disabled = false
        }
      })
    }
    
  } 

  removeStratBtn() {
    const removeStart = document.querySelector('.start');
    if(removeStart) {      
      App.rootElement.removeChild(removeStart)
    }
  }

}

export default FightersView;
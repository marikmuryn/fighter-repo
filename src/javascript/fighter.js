class Fighter {
    constructor(name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    randomHit(from, to) {
        return Math.floor(Math.random() * (to - from) ) + from;
    }

    getHitPower() {
        const power = this.attack * this.randomHit(1,3); 
        return power;
    }

    getBlockPower() {
        const power = this.defense * this.randomHit(1,3);
        return power; 
    }

    static hit(fighter1, fighter2) {
        const hitFighetr1 = fighter1.getHitPower();
        const blockFighetr2 = fighter2.getBlockPower();
        let healthFighter2;
        if(blockFighetr2 > hitFighetr1) {
            healthFighter2 = 0;
        }else {
            healthFighter2 = hitFighetr1 - blockFighetr2; 
        }
        const resultHealthFighter = fighter2.health - healthFighter2;

        return resultHealthFighter;
    }

    static fight(fighter1, fighter2) {

        let isDead = false;
        while(!isDead) {

            let health2 = Fighter.hit(fighter1,fighter2);
            if( health2 <= 0 ){
                isDead = true;
                return fighter1.name;
            }
            fighter2.health = health2;

            let health1 = Fighter.hit(fighter2,fighter1);
            if( health1 <= 0 ){
                isDead = true;
                return fighter2.name;
            }
            fighter1.health = health1;
        }
    }
}


export default Fighter;